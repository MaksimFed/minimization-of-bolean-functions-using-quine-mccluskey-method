import minimization.Minimizer;


public class Main {
    public static void main(String[] args) {
        final  int NUMBER_OF_VARIABLES = 4;
        final  int [] ONE_VALUE_SETS = {0,1,6,7,8,11,12,14};
        //final  int NUMBER_OF_VARIABLES = 4;
        //final  int [] ONE_VALUE_SETS = {4,8,9,10,11,12,14,15};

        Minimizer minimizer = new Minimizer(NUMBER_OF_VARIABLES,ONE_VALUE_SETS);
        minimizer.minimize();

    }
}
