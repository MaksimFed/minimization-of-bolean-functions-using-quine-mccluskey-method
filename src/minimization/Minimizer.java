package minimization;

import minimization.method.petrick.PetrickMethodSimplifier;
import minimization.utils.Minterm;
import minimization.utils.MintermGroup;

import java.util.*;

public class Minimizer {

    private List<Minterm> CDNF;
    private int NUMBER_OF_VARIABLES;
    private Set<Minterm> ungluedMinterms;

    public Minimizer(final int NUMBER_OF_VARIABLES,  final  int[] ONE_VALUE_SETS) {

        int [] mintermValues = new int[NUMBER_OF_VARIABLES];
        CDNF = new ArrayList<>(ONE_VALUE_SETS.length);
        this.NUMBER_OF_VARIABLES = NUMBER_OF_VARIABLES;
        ungluedMinterms = new HashSet<>();

        for (int i = 0; i < ONE_VALUE_SETS.length; i++){
            int temp = ONE_VALUE_SETS[i];

            for (int j = 0; j < mintermValues.length; j++){
                mintermValues[mintermValues.length - j - 1] = (temp >> j)%2;

            }

            CDNF.add(new Minterm(mintermValues));

        }

    }

    public void minimize() {
        List<MintermGroup> currentRank = new ArrayList<>(NUMBER_OF_VARIABLES + 1);
        for (int i = 0; i < NUMBER_OF_VARIABLES + 1 ; i++){
            currentRank.add(new MintermGroup());
        }


        for (Minterm cdnfIterator : CDNF){
            int numberOfOnes = cdnfIterator.getNumberOfOnes();
            currentRank.get(numberOfOnes).add(cdnfIterator);
        }


        for (int i = 0; i < NUMBER_OF_VARIABLES + 1; i++){

            currentRank = iteration(currentRank);

        }

        System.out.println(ungluedMinterms);

        PetrickMethodSimplifier petrickMethodSimplifier= new PetrickMethodSimplifier(CDNF,ungluedMinterms);
        petrickMethodSimplifier.simplify();
        System.out.println("simplified result is " + petrickMethodSimplifier.getSimplifyResult());
    }

    private List<MintermGroup> iteration(List<MintermGroup> currentRank) {
        List<MintermGroup> nextRank = new ArrayList<>();

        for (int i = 0; i < currentRank.size() - 1; i++){

            nextRank.add(new MintermGroup());

            Iterator<Minterm> firstIterator = currentRank.get(i).getMinterms().iterator();
            Iterator<Minterm> secondIterator = currentRank.get(i + 1).getMinterms().iterator();

            while (firstIterator.hasNext()){
                Minterm firstMinterm = firstIterator.next();
                secondIterator = currentRank.get(i + 1).getMinterms().iterator();
                while (secondIterator.hasNext()){
                    Minterm secondMinterm = secondIterator.next();

                    if (firstMinterm.isGlueable(secondMinterm)){

                        try {
                            nextRank.get(i).add(firstMinterm.glue(secondMinterm));
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        firstMinterm.setGlued(true);
                        secondMinterm.setGlued(true);
                    }

                }
            }

        }

        for (MintermGroup mintermGroupIterator : currentRank){
            Iterator<Minterm> groupIterator = mintermGroupIterator.getMinterms().iterator();
            while (groupIterator.hasNext()){
                Minterm temp = groupIterator.next();
                if (!temp.isGlued()){
                    ungluedMinterms.add(temp);
                }

            }
        }

        return nextRank;
    }
}
