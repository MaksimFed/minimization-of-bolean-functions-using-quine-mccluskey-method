package minimization.method.petrick;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Conjunct {
    private Set<Integer> conjunctParts;

    public Conjunct(int value){
        conjunctParts = new TreeSet<>();
        conjunctParts.add(value);
    }

    public Conjunct() {
        conjunctParts = new TreeSet<>();
    }

    public int size() {
        return conjunctParts.size();
    }

    public void addConjunct(Conjunct conjunct) {
        for (Integer additionalConjunct : conjunct.getConjunctParts()){
            conjunctParts.add(additionalConjunct);
        }
    }

    public Set<Integer> getConjunctParts() {
        return conjunctParts;
    }

    public boolean isEqual(Conjunct newConjuct) {
        if (conjunctParts.size() != newConjuct.size()){
            return false;
        }
        Iterator<Integer> conjunctPartsIterator = conjunctParts.iterator();
        Iterator<Integer> newConjunctIterator = newConjuct.getConjunctParts().iterator();

        while (conjunctPartsIterator.hasNext() && newConjunctIterator.hasNext()){

            if (conjunctPartsIterator.next().compareTo(newConjunctIterator.next()) != 0){
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString(){
        return conjunctParts.toString();
    }
}
