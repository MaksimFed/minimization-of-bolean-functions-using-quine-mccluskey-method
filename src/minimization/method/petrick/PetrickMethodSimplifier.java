package minimization.method.petrick;

import minimization.utils.Minterm;

import java.util.*;

public class PetrickMethodSimplifier {

    private List<SumOfProducts> sumOfProductsGroups;
    private Collection<Minterm> simplifiedMinterms;
    private Collection<Minterm> ungluedMinterms;
    Conjunct minConjunct;

    public PetrickMethodSimplifier(Collection<Minterm> CDNF,Collection<Minterm> ungluedMinterms){
        System.out.println("CDNF:");
        System.out.println(CDNF);
        sumOfProductsGroups = new LinkedList<SumOfProducts>();
        this.ungluedMinterms = ungluedMinterms;
        for (int i = 0; i < CDNF.size(); i++){
            sumOfProductsGroups.add(new SumOfProducts());
        }
        formSumofProducts(CDNF,ungluedMinterms);


    }



    public void simplify(){

        while (sumOfProductsGroups.size() != 1){

            sumOfProductsGroups.get(0).multiplySumOfProducts(sumOfProductsGroups.get(1));
            sumOfProductsGroups.remove(1);

        }

        SumOfProducts lastSumOfProducts = sumOfProductsGroups.get(0);
        int minSize = lastSumOfProducts.getConjunct(0).size();
        Conjunct minConjunct = lastSumOfProducts.getConjunct(0);

        for (int i = 1; i < lastSumOfProducts.size(); i++){
            if (lastSumOfProducts.getConjunct(i).size() < minSize){
                minSize = lastSumOfProducts.getConjunct(i).size();
                minConjunct = lastSumOfProducts.getConjunct(i);
            }
        }

        this.minConjunct = minConjunct;
        System.out.println("min conjuct is:" + minConjunct);
    }

    private void formSumofProducts(Collection<Minterm> CDNF, Collection<Minterm> ungluedMinterms) {
        Iterator<Minterm> ungluedMintermsIterator = ungluedMinterms.iterator();
        int i = 0;

        while (ungluedMintermsIterator.hasNext()){
            Minterm currentUngluedMinterm = ungluedMintermsIterator.next();
            Iterator<Minterm> cdnfIterator = CDNF.iterator();
            int j = 0;

            while (cdnfIterator.hasNext()){
                Minterm currentCDNFMinterm = cdnfIterator.next();

                if (currentUngluedMinterm.isPartOf(currentCDNFMinterm)){
                    sumOfProductsGroups.get(j).addConjunct(i);
                }
                j++;
            }

            i++;
        }

    }

    public Collection<Minterm> getSimplifyResult() {
        Collection<Minterm> result = new ArrayList<>();
        Collection<Integer> indexes = minConjunct.getConjunctParts();

        Iterator<Integer> indexIterator = indexes.iterator();

        while (indexIterator.hasNext()){
            Integer currentIndex = indexIterator.next();
            int i = 0;
            int end = currentIndex;
            Minterm foundedMinterm;
            Iterator<Minterm> ungluedMintermIterator = ungluedMinterms.iterator();
            while (ungluedMintermIterator.hasNext()){
                foundedMinterm = ungluedMintermIterator.next();
                if (currentIndex.compareTo(i) == 0){
                    result.add(foundedMinterm);
                }
                i++;
            }
        }
        return result;
    }
}
