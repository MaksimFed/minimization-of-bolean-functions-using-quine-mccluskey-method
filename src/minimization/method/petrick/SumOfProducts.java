package minimization.method.petrick;


import java.util.ArrayList;
import java.util.List;

public class SumOfProducts {

    private List<Conjunct> conjuncts;

    public SumOfProducts(){
        conjuncts = new ArrayList<>();
    }

    public void multiplySumOfProducts(SumOfProducts sumOfProducts) {
        List<Conjunct> newConjunct = new ArrayList<>();
        for (int i = 0; i < conjuncts.size(); i++){
            for (int j = 0; j < sumOfProducts.size(); j++){
                Conjunct resultOfMultiply = new Conjunct();
                resultOfMultiply.addConjunct(conjuncts.get(i));
                resultOfMultiply.addConjunct(sumOfProducts.getConjunct(j));
                addToNewConjuct(newConjunct,resultOfMultiply);
            }

        }

        conjuncts = newConjunct;
    }

    private void addToNewConjuct(List<Conjunct> newConjunct, Conjunct resultOfMultiply) {
        for (Conjunct newConjuctIterator : newConjunct){
            if (resultOfMultiply.isEqual(newConjuctIterator)){
                return;
            }
        }
        newConjunct.add(resultOfMultiply);
    }

    public Conjunct getConjunct(int i) {
        return  conjuncts.get(i);
    }

    public void addConjunct(int i) {
        conjuncts.add(new Conjunct(i));
    }

    public int size() {
        return  conjuncts.size();
    }

    @Override
    public String toString(){

        return conjuncts.toString();
    }
}
