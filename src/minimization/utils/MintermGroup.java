package minimization.utils;

import java.util.*;

public class MintermGroup {

    private Set<Minterm> minterms;

    public MintermGroup(){
        minterms = new HashSet<>();
    }

    public void add(Minterm minterm) {

        Iterator<Minterm> mintermIterator = minterms.iterator();

        while (mintermIterator.hasNext()){
            Minterm currentMinterm = mintermIterator.next();
            int i = 0;
            for (i = 0; i < minterm.getMintermParts().size(); i++){
                if (currentMinterm.getMintermParts().get(i).compareTo(minterm.getMintermParts().get(i)) != 0){
                    break;
                }
            }
            if (i == minterm.getMintermParts().size()){
                return;
            }
        }

        minterms.add(minterm);
    }

    @Override
    public String toString(){
        return minterms.toString();
    }

    public Set<Minterm> getMinterms() {
        return minterms;
    }
}
