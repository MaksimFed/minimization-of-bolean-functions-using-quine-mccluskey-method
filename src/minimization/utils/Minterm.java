package minimization.utils;


import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Minterm implements Serializable{

    private List<Integer> mintermParts;
    private boolean glued;

    public Minterm(int[] mintermValues) {
        mintermParts = new ArrayList<>(mintermValues.length);
        glued = false;

        for (int i = 0; i < mintermValues.length; i++){
            mintermParts.add(mintermValues[i]);
        }
    }

    @Override
    public String toString(){
        StringBuilder buffer  = new StringBuilder();
        for (int i = 0; i < mintermParts.size(); i++){

            if (mintermParts.get(i) == 0){
                //buffer.append("!");
                //buffer.append("X");
                //buffer.append(i);
                //buffer.append("_");
                buffer.append("0");
            }
            else if (mintermParts.get(i) != 1){
                //buffer.append("-");
                //buffer.append("_");
                buffer.append("-");
            }
            else{
                //buffer.append("X");
                //buffer.append(i);
                //buffer.append("_");
                buffer.append("1");

            }


        }

        return buffer.toString();
    }

    public int getNumberOfOnes() {
        int numberOfOnes = 0;
        for (Integer mintermPartsIterator : mintermParts){
            if (mintermPartsIterator == 1){
                numberOfOnes++;
            }
        }

        return numberOfOnes;
    }

    public boolean isGlued() {
        return glued;
    }

    public void setGlued(boolean glued) {
        this.glued = glued;
    }

    public List<Integer> getMintermParts() {
        return mintermParts;
    }

    public boolean isGlueable(Minterm secondMinterm) {

        List <Integer> secondMintermParts = secondMinterm.getMintermParts();
        int numberOfDifferentParts = 0;

        for (int i = 0; i < mintermParts.size(); i++){
            if (mintermParts.get(i).compareTo(secondMintermParts.get(i)) != 0){
                numberOfDifferentParts++;
            }
        }

        if (numberOfDifferentParts != 1){
            return false;
        }
        return true;
    }



    public Minterm glue(Minterm secondMinterm) throws IOException, ClassNotFoundException {

        Minterm newMinterm = null;
        List<Integer> secondMintermParts = secondMinterm.getMintermParts();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(this);
        objectOutputStream.close();

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        newMinterm = (Minterm) objectInputStream.readObject();

        newMinterm.setGlued(false);

        for (int i = 0; i < mintermParts.size(); i++){
            if (mintermParts.get(i).compareTo(secondMintermParts.get(i)) != 0 ){
                newMinterm.getMintermParts().remove(i);
                newMinterm.getMintermParts().add(i,-1);
                break;
            }
        }

        return newMinterm;
    }

    public boolean isPartOf(Minterm currentCDNFMinterm) {
        for (int i = 0; i < mintermParts.size(); i++){
            if (mintermParts.get(i).compareTo(-1) != 0
                    && mintermParts.get(i).compareTo(currentCDNFMinterm.getMintermParts().get(i)) != 0){
                return false;
            }
        }
        return true;
    }

    /*
    @Override
    public int hashCode(){
        int hash = 0;
        for (int i = 0; i < mintermParts.size(); i++){
            if (mintermParts.get(i).compareTo(0) == 0){
                hash <<= 2;
            }
            if (mintermParts.get(i).compareTo(1) == 0){
                hash <<= 2;
                hash |= 1;
            }
            if (mintermParts.get(i).compareTo(-1) == 0){
                hash <<= 2;
                hash |= 3;
            }
        }

        return  hash;
    }*/
}
